# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 1.4.3"
VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
  end
  # master node configuration
  config.vm.define "spark-master-node" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "spark-master-node"
    c.vm.network "private_network", ip: "10.20.1.100"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/java.sh"
    c.vm.provision "shell", path: "shell/spark.sh"
    c.vm.provision "shell", path: "shell/hadoop.sh"
  end
  # slave node1 configuration
  config.vm.define "spark-slave-node1" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "spark-slave-node1"
    c.vm.network "private_network", ip: "10.20.1.101"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/java.sh"
    c.vm.provision "shell", path: "shell/spark.sh"
    c.vm.provision "shell", path: "shell/hadoop.sh"
  end
  # slave node2 configuration
  config.vm.define "spark-slave-node2" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "spark-slave-node2"
    c.vm.network "private_network", ip: "10.20.1.102"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/java.sh"
    c.vm.provision "shell", path: "shell/spark.sh"
    c.vm.provision "shell", path: "shell/hadoop.sh"
  end
  # slave node3 configuration
  config.vm.define "spark-slave-node3" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "spark-slave-node3"
    c.vm.network "private_network", ip: "10.20.1.103"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/java.sh"
    c.vm.provision "shell", path: "shell/spark.sh"
    c.vm.provision "shell", path: "shell/hadoop.sh"
  end
  # activemq
  config.vm.define "activemq" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "activemq"
    c.vm.network "private_network", ip: "10.20.1.110"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/java.sh"
    c.vm.provision "shell", path: "shell/activemq.sh"
  end
  # postgresql
  config.vm.define "postgresql" do |c|
    c.vm.box = "centos65"
    c.vm.box_url = "https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box"
    c.vm.hostname = "postgresql"
    c.vm.network "private_network", ip: "10.20.1.110"
    c.vm.synced_folder "./downloads", "/vagrant/downloads"
    c.vm.synced_folder "./files", "/vagrant/files"
    c.vm.provider "virtualbox" do |vb|
      vb.gui = false
      vb.memory = "2048"
    end
    c.vm.provision "shell", path: "shell/bootstrap.sh"
    c.vm.provision "shell", path: "shell/postgresql.sh"
  end
end
