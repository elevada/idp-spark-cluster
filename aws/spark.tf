#
# Spark master
#
resource "aws_instance" "spark-node" {
    ami = "${var.ami_centos}"
    availability_zone = "us-west-2c"
    instance_type = "${var.instance_type_spark}"
    key_name = "${var.key_name_spark}"
    private_ip = "172.31.5.5${count.index}"
    vpc_security_group_ids = [
        "${aws_security_group.spark-node.id}"
    ]
    root_block_device {
        volume_type = "gp2"
        volume_size = "30"
    }
    tags {
        Name = "spark-node-${count.index}"
    }
    count = 4
}

resource "aws_security_group" "spark-node" {
    name = "spark-node"
    description = "Spark Master Instance Security Group"
    tags {
        Name = "spark-node"
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 8080
        to_port = 8081
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 8089
        to_port = 8089
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 8042
        to_port = 8042
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 8088
        to_port = 8088
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 19888
        to_port = 19888
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 50070
        to_port = 50070
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 50075
        to_port = 50075
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "172.31.0.0/16"
        ]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}