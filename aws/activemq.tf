#
# Spark master
#
resource "aws_instance" "activemq" {
    ami = "${var.ami_centos}"
    availability_zone = "us-west-2c"
    instance_type = "${var.instance_type_activemq}"
    key_name = "${var.key_name_activemq}"
    private_ip = "172.31.5.60"
    vpc_security_group_ids = [
        "${aws_security_group.activemq.id}"
    ]
    root_block_device {
        volume_type = "gp2"
        volume_size = "30"
    }
    tags {
        Name = "activemq"
    }
}

resource "aws_security_group" "activemq" {
    name = "activemq"
    description = "ActiveMQ Instance Security Group"
    tags {
        Name = "activemq"
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 8161
        to_port = 8161
        protocol = "tcp"
        cidr_blocks = [
            "${var.ip_ernesto}/32",
            "${var.ip_javier}/32",
            "${var.ip_peter}/32"
        ]
    }
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "172.31.0.0/16"
        ]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}