#!/bin/bash
bootstraped="/tmp/vagrant.bootstraped"
if [ -f "$bootstraped" ]
then
    echo "this node has already been bootstraped, file found: $bootstraped"
else

    echo "update and upgrade"
    yum update -y
    yum upgrade -y

    echo "install global required packages"
    yum install -y vim wget git

    echo "disable iptables"
    service iptables save
    service iptables stop
    chkconfig iptables off

    echo "install dependencies to compile python2.7"
    yum groupinstall -y development
    yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel xz-libs glibc.i686
    wget https://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz
    tar xzf Python-2.7.11.tgz
    cd Python-2.7.11
    ./configure --prefix=/usr/local
    make
    make altinstall

    echo "installing setuptools"
    wget --no-check-certificate https://pypi.python.org/packages/source/s/setuptools/setuptools-1.4.2.tar.gz
    tar -xvf setuptools-1.4.2.tar.gz
    cd setuptools-1.4.2
    python2.7 setup.py install

    echo "install pip2.7"
    curl https://bootstrap.pypa.io/get-pip.py | python2.7 -

    echo "install required pip2.7 libraries"
    /usr/local/bin/pip2.7 install findspark
    /usr/local/bin/pip2.7 install IPython
    /usr/local/bin/pip2.7 install unittest2
    /usr/local/bin/pip2.7 install slacker
    /usr/local/bin/pip2.7 install boto3
    /usr/local/bin/pip2.7 install stompy

    echo "set /etc/hosts file"
    cp -f /home/centos/idp-spark-cluster/files/etc/hosts /etc/hosts

    echo "set up root private and public keys"
    mkdir /root/.ssh
    cp -f /home/centos/idp-spark-cluster/files/ssh/id_rsa /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa
    cp -f /home/centos/idp-spark-cluster/files/ssh/id_rsa.pub /root/.ssh/id_rsa.pub
    cp -f /home/centos/idp-spark-cluster/files/ssh/authorized_keys /root/.ssh/authorized_keys

    touch "$bootstraped"
fi
