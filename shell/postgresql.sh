#!/bin/bash
postgresql="/tmp/vagrant.postgresql"
if [ -f "$postgresql" ]
then
    echo "this node has already been postgresql, file found: $postgresql"
else
    echo "install and configure postgresql"
    yum install postgresql-server -y
    service postgresql initdb
    chkconfig postgresql on
    service postgresql start
    echo "CREATE DATABASE idp_demo" | sudo -u postgres psql postgres
    sudo -u postgres psql -d idp_demo -f /home/centos/idp-spark-cluster/files/postgresql/transformation_set.sql
    touch "$postgresql"
fi