# first configure terraform
terraform plan
terraform
# then configure ansible and run
ansible spark -m shell -a 'yum install -y git wget vim epel-release' -sudo
ansible spark -m copy -a 'src=files/ssh/id_rsa dest=/home/centos/.ssh owner=centos group=centos mode=0600'
ansible spark -m copy -a 'src=files/ssh/id_rsa.pub dest=/home/centos/.ssh owner=centos group=centos mode=0644'
ansible spark -m copy -a 'src=files/ssh/known_hosts dest=/home/centos/.ssh owner=centos group=centos mode=0644'
ansible spark -m copy -a 'src=files/etc/hosts.aws dest=/etc/hosts owner=root group=root mode=0644' -sudo
ansible spark -m shell -a 'git clone git@bitbucket.org:elevada/idp-spark-cluster.git'
ansible spark -m shell -a 'git clone git@bitbucket.org:elevada/idp.git'
ansible spark -m copy -a 'src=files/idp.conf dest=/home/centos/idp/idp/idp.conf owner=centos group=centos mode=0644'
ansible spark -m copy -a 'src=files/etc/yum.repos.d/s3tools.repo dest=/etc/yum.repos.d/s3tools.repo owner=root group=root mode=0644' -sudo
ansible spark -m shell -a 'yum --enablerepo epel-testing -y install s3cmd' -sudo
ansible spark -m shell -a 'yum -y install python-pip' -sudo
ansible spark -m copy -a 'src=files/.s3cfg dest=/home/centos/.s3cfg owner=centos group=centos mode=0600'
ansible spark -m shell -a 's3cmd get s3://elev-idp/downloads/hadoop-2.6.4.tar.gz /home/centos/idp-spark-cluster/downloads/'
ansible spark -m shell -a 's3cmd get s3://elev-idp/downloads/jdk-8u74-linux-i586.tar.gz /home/centos/idp-spark-cluster/downloads/'
ansible spark -m shell -a 's3cmd get s3://elev-idp/downloads/spark-1.6.1-bin-hadoop2.6.tgz /home/centos/idp-spark-cluster/downloads/'
ansible spark -m shell -a '/home/centos/idp-spark-cluster/shell/bootstrap7.sh' -sudo
ansible spark -m shell -a '/home/centos/idp-spark-cluster/shell/java.sh' -sudo
ansible spark -m shell -a '/home/centos/idp-spark-cluster/shell/spark.sh' -sudo
ansible spark -m shell -a '/home/centos/idp-spark-cluster/shell/hadoop.sh' -sudo
ansible spark-node-0 -m shell -a '/usr/local/hadoop/bin/hdfs namenode -format myhadoop' -sudo
ansible spark-node-0 -m shell -a '/usr/local/hadoop/sbin/hadoop-daemon.sh --config /usr/local/hadoop/etc/hadoop --script hdfs start namenode' -sudo
ansible spark-node-0 -m shell -a '/usr/local/hadoop/sbin/hadoop-daemons.sh --config /usr/local/hadoop/etc/hadoop --script hdfs start datanode' -sudo
ansible spark-node-0 -m shell -a '/usr/local/spark/sbin/start-all.sh' -sudo
ansible spark -m shell -a 'mkdir /home/centos/tmp'
ansible spark -m shell -a 'mkdir /home/centos/workdir'
ansible spark -m shell -a 'mkdir /home/centos/.aws'
ansible spark -m copy -a 'src=files/.aws/config dest=/home/centos/.aws/config owner=centos group=centos mode=0600'
ansible spark -m copy -a 'src=files/.aws/credentials dest=/home/centos/.aws/credentials owner=centos group=centos mode=0600'
#yarn
#/usr/local/hadoop/sbin/yarn-daemon.sh --config /usr/local/hadoop/etc/hadoop start resourcemanager
#/usr/local/hadoop/sbin/yarn-daemons.sh --config /usr/local/hadoop/etc/hadoop start nodemanager
#/usr/local/hadoop/sbin/yarn-daemon.sh start proxyserver --config /usr/local/hadoop/etc/hadoop
#/usr/local/hadoop/sbin/mr-jobhistory-daemon.sh start historyserver --config /usr/local/hadoop/etc/hadoop
