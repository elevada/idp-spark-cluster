#!/bin/bash
bootstraped="/tmp/vagrant.bootstraped"
if [ -f "$bootstraped" ]
then
    echo "this node has already been bootstraped, file found: $bootstraped"
else

    echo "update and upgrade"
    yum update -y
    yum upgrade -y

    echo "install global required packages"
    yum install -y vim wget git zip
    yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel xz-libs glibc.i686

    echo "disable firewall"
    systemctl disable firewalld
    systemctl stop firewalld

    echo "install pip"
    yum -y install python-pip
    pip install --upgrade pip

    echo "install required pip libraries"
    pip install findspark
    pip install IPython
    pip install unittest2
    pip install slacker
    pip install boto3
    pip install stompy
    pip install pywebhdfs

    echo "set /etc/hosts file"
    cp -f /home/centos/idp-spark-cluster/files/etc/hosts.aws /etc/hosts

    echo "set up root private and public keys"
    cp -f /home/centos/idp-spark-cluster/files/ssh/id_rsa /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa
    cp -f /home/centos/idp-spark-cluster/files/ssh/id_rsa.pub /root/.ssh/id_rsa.pub
    cat /home/centos/idp-spark-cluster/files/ssh/authorized_keys >> /root/.ssh/authorized_keys

    touch "$bootstraped"
fi
